package dominik.kedziak.willitraintoday

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import java.io.IOException
import java.io.InputStream

class FetchCities(var context: Context): AsyncTask<Void, Void, ArrayList<String>>(){

    private var response = context as CitiesResponse

    override fun doInBackground(vararg params: Void?): ArrayList<String> {
        val cities = ArrayList<String>()
        try {
            val stream = context.resources.openRawResource(R.raw.city_list) as InputStream
            val size = stream.available()
            val buffer = ByteArray(size)
            stream.read(buffer)
            stream.close()
            val json = String(buffer)

            val obj = JSONArray(json)
            println(obj.length())
            val jsonLength = obj.length()
            for (i in 0 until jsonLength){
                cities.add(obj.get(i).toString())
            }


            Log.v("AsyncTask", "executing task completed")
        } catch (e: IOException){
            Log.e("IOEception", e.toString())
        } catch (e: OutOfMemoryError){
            Log.e("No memory", e.toString())
        } catch (e: RuntimeException){
            Log.e("Cities nullability", e.toString())
        } catch (e: JSONException){
            Log.e("JSON", e.toString())
        }
        return cities
    }

    override fun onPostExecute(result: ArrayList<String>?) {
        super.onPostExecute(result)
        response.processFinish(result)
        Log.v("AsyncTask", "sending data to interface")

    }
}