package dominik.kedziak.willitraintoday

import com.google.gson.JsonElement
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GetWeather {

    //What we want to achieve: http://api.openweathermap.org/data/2.5/forecast?q={city}&appid={appId}
    @GET("forecast") // --> http://api.openweathermap.org/data/2.5/forecast?
    fun getWeather(
        @Query("q") city: String, // --> http://api.openweathermap.org/data/2.5/forecast?q{city}
        @Query("APPID") api: String) // --> http://api.openweathermap.org/data/2.5/forecast?q{city}&appid={appid}
            : Call<JsonElement>

}