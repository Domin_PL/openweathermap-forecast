package dominik.kedziak.willitraintoday

import android.app.Application
import com.squareup.leakcanary.LeakCanary

class LeakCanary : Application() {

    override fun onCreate() {
        super.onCreate()
        if (LeakCanary.isInAnalyzerProcess(this)){
            return
        }
        LeakCanary.install(this)
    }
}