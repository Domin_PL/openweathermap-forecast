package dominik.kedziak.willitraintoday

import android.annotation.SuppressLint
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.*
import com.google.gson.JsonElement
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.text.DecimalFormat


class MainActivity : AppCompatActivity(), CitiesResponse{


    private lateinit var citySearchAutoCompleteTextView: AutoCompleteTextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fetchWeather()

        val fetchData = FetchCities(this)
        fetchData.execute()

        citySearchAutoCompleteTextView = findViewById(R.id.citySearchEditText)

        val saveBtn: Button = findViewById(R.id.searchButton)
        saveBtn.setOnClickListener {
            if (!citySearchAutoCompleteTextView.text.toString().isEmpty()) {
                val city = citySearchAutoCompleteTextView.text.toString()
                val sp = PreferenceManager.getDefaultSharedPreferences(this)
                sp.edit().putString("city_code", city).apply()
                fetchWeather()
            } else {
                Toast.makeText(this, "City is empty", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun processFinish(result: ArrayList<String>?) {
        try {
            if (result != null){
                val adapter = ArrayAdapter<String>(applicationContext, android.R.layout.simple_list_item_1, result)
                citySearchAutoCompleteTextView.setAdapter(adapter)
                Log.v("Adapter", "adapter set up")
            }
        } catch (e: IOException){
            Log.e("Error", e.toString())
        } catch (e: OutOfMemoryError){
            Log.e("Lack of memory", e.toString())
        }
    }


    @SuppressLint("SetTextI18n")
    fun updateUi(weather: Weather?) {
        val temperature = findViewById<TextView>(R.id.temperature_textview)
        if (weather != null) {
            temperature.text = weather.temp
            val cityName: TextView = findViewById(R.id.cityTextView)
            cityName.text = weather.city
            val valsTxt: TextView = findViewById(R.id.valsTextView)
            valsTxt.text = weather.tempMin + " min " + weather.tempMax + " max " +
                    weather.pressure + " pressure " + weather.humidity + " humidity " +
                    weather.cloudiness + " cloudiness " + weather.windSpeed + " windspeed"
        }
    }

    fun fetchWeather(){
       
        TODO("Add your OpenWeatherMap API KEY")
        val api = ""

        val retrofit = Retrofit.Builder()
            .baseUrl("http://api.openweathermap.org/data/2.5/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val sp = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        val code = sp.getString("city_code", "New York,US")
        val jsonPlaceholder = retrofit.create(GetWeather::class.java)
        val call = jsonPlaceholder.getWeather(code, api)
        call.enqueue(object : Callback<JsonElement>{
            override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                val url  = call.request().url()
                Log.e("Error", t.message)
                Log.v("URL", url.toString())
            }

            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                try {
                    val url  = call.request().url()
                    Log.v("URL", url.toString())
                    if (!response.isSuccessful){
                        Log.e("Connection failed", response.code().toString())
                    } else {
                        val unit = response.body()
                        if (unit != null){
                            Log.v("Unit", unit.toString())
                            val jsonObject = JSONObject(unit.toString())
                            val featureArray = jsonObject.getJSONArray("list")

                            if (featureArray.length() > 0) {
                                val list = featureArray.getJSONObject(0)
                                val celsiusConversion = 273.15
                                //get temperature --> list.main.temp
                                val main = list.getJSONObject("main")
                                var temp = main.getString("temp")
                                var celsiusTemp = temp.toDouble()
                                celsiusTemp -= celsiusConversion
                                temp = DecimalFormat("##.#").format(celsiusTemp).toString() + "°C"

                                //get minimum temperature --> list.main.temp_min
                                var tempMin = main.getString("temp_min")
                                var celsiusTempMin = tempMin.toDouble()
                                celsiusTempMin -= celsiusConversion
                                tempMin = DecimalFormat("##.#").format(celsiusTempMin).toString() + "°C"

                                //get maximum temperature --> list.main.temp_max
                                var tempMax = main.getString("temp_max")
                                var celsiusTempMax = tempMax.toDouble()
                                celsiusTempMax -= celsiusConversion
                                tempMax = DecimalFormat("##.#").format(celsiusTempMax).toString() + "°C"

                                //get pressure --> list.main.pressure
                                val pressure = main.getString("pressure")

                                //get humidity --> list.main.humidity
                                val humidity = main.getString("humidity")

                                //get cloudiness --> list.clouds.all
                                val clouds = list.getJSONObject("clouds")
                                val cloudiness = clouds.getString("all")

                                //get windSpeed --> list.wind.speed
                                val wind = list.getJSONObject("wind")
                                val windSpeed = wind.getString("speed")

                                //city name --> city.name
                                val cityName = jsonObject.getJSONObject("city")
                                val city = cityName.getString("name")

                                val weather = Weather(temp, city, tempMin, tempMax, pressure,
                                    humidity, cloudiness, windSpeed)
                                updateUi(weather)
                            }
                        }
                    }
                } catch (e: KotlinNullPointerException){
                    Log.e("Unit is null", e.toString())
                }
            }
        })
    }
}

