package dominik.kedziak.willitraintoday

data class Weather(
    val temp: String,
    val city: String,
    val tempMin: String,
    val tempMax: String,
    val pressure: String,
    val humidity: String,
    val cloudiness: String,
    val windSpeed: String
)